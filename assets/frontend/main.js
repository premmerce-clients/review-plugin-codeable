jQuery(document).ready(function ($) {

    if (reviewsConfig === undefined) {
        return;
    }

    $('[data-create-review-form]').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
                url: reviewsConfig.ajaxUrl,
                type: 'POST',
                data: $(this).serialize(),
                success: function (data) {
                    console.log(data);
                }
            }
        );
    });

    var loadPage = 2;
    var needLoadMore = true;
    var orderBy = 'date';

    jQuery('[data-review-load-more]').click(function (e) {
        e.preventDefault();
        if (needLoadMore) {
            $.ajax({
                    url: reviewsConfig.ajaxUrl,
                    type: 'POST',
                    data: {
                        'action': reviewsConfig.loadMoreAction,
                        'loadPage': loadPage,
                        'postId': reviewsConfig.currentPostId,
                        'orderBy': orderBy
                    },
                    success: function (data) {
                        if (data.needLoadMore) {
                            loadPage++;
                            $('[data-reviews]').append(data.reviewsHtml);
                        } else {
                            needLoadMore = false;
                        }
                    }
                }
            );
        }
    });


    jQuery(document).on('click', '[data-review-like-button]', function (e) {
        e.preventDefault();
        var button = $(this);

        // TODO CHECK IF BUTTON DISABLED AND DO NOT SEND REQUEST
        $.ajax({
                url: reviewsConfig.ajaxUrl,
                type: 'POST',
                data: {
                    'action': reviewsConfig.likeReviewAction,
                    'postId': button.data('review-like-id'),
                },
                success: function (data) {
                    // TODO UPDATE LIKES COUNT
                    button.attr('disabled', true);
                }
            }
        );

    })
});