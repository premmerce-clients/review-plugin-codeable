<?php use Codeable\Reviews\Review\CPT;

defined( "ABSPATH" ) || die;
/**
 * @var bool $enableOnPages
 * @var bool $enableOnPosts
 * @var string $googleSiteKey
 * @var string $googleSiteSecret
 */

?>

<div class="wrap">

    <h1><?php _e( 'Review Settings', 'review' ) ?></h1>

    <form method="post" action="<?php echo admin_url( 'admin-post.php' ) ?>">

        <input type="hidden" name="action" value="<?php echo CPT::SETTING_ACTION ?>">

        <table class="form-table">
            <tbody>

            <tr>
                <th><h3><?php _e( 'Displaying', 'reviews' ); ?></h3></th>
            </tr>

            <tr>
                <th>
                    <label for="reviews_enable_on_pages"><?php _e( 'Enable on pages', 'reviews' ) ?></label>
                </th>
                <td>
                    <input id="reviews_enable_on_pages" type="checkbox"
                           name="reviews_enable_on_pages" <?php checked( $enableOnPages ); ?>>
                </td>
            </tr>

            <tr>
                <th>
                    <label for="reviews_enable_on_posts"><?php _e( 'Enable on posts', 'reviews' ) ?></label>
                </th>
                <td>
                    <input id="reviews_enable_on_posts" type="checkbox"
                           name="reviews_enable_on_posts" <?php checked( $enableOnPosts ); ?>>
                </td>
            </tr>

            <tr>
                <th><h3><?php _e( 'Captcha', 'reviews' ); ?></h3></th>
            </tr>

            <tr>
                <th>
                    <label for="reviews_google_site_key"><?php _e( 'Google site key', 'reviews' ) ?></label>
                </th>
                <td>
                    <input style="width: 25%;" id="reviews_google_site_key" type="text"
                           name="reviews_google_site_key" value="<?php echo $googleSiteKey; ?>">
                </td>
            </tr>

            <tr>
                <th>
                    <label for="reviews_google_site_secret"><?php _e( 'Google site secret', 'reviews' ) ?></label>
                </th>
                <td>
                    <input style="width: 25%;" id="reviews_google_site_secret" type="text"
                           name="reviews_google_site_secret" value="<?php echo $googleSiteSecret; ?>">
                </td>
            </tr>

			<?php wp_nonce_field( CPT::SETTING_ACTION ); ?>

            </tbody>
        </table>

		<?php submit_button( __( 'Save' ) ); ?>
    </form>

</div>