<?php defined( "ABSPATH" ) || die;

use Codeable\Reviews\AjaxHandler;
use Codeable\Reviews\Review\Review;
use Premmerce\SDK\V2\FileManager\FileManager;

/**
 * @var Review[] $reviews
 * @var float $averageRating
 * @var FileManager $fileManager
 * @var string $googleSiteKey
 * @var array $totals
 */
?>

<script>

</script>
<style>
    .reviews-container {
        margin-top: 20px;
        background: #fff;
        box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, .15);
        border-radius: 15px;
        padding: 28px 40px 40px 40px;
    }

    .review {
        border: 1px solid #ccc;
        border-radius: 15px;
        box-shadow: 0px 0px 6px 0px rgba(0, 0, 0, .15);
        margin-bottom: 10px;
        padding: 28px 40px 40px 40px;
    }
</style>
<div class="reviews-container">
    <h4>Average rating: <?php echo $averageRating; ?></h4>


    <?php foreach ( $totals as $key => $total ): ?>
        <p><?php echo $key; ?>: <?php echo $total; ?></p>
	<?php endforeach; ?>


    <div class="reviews" data-reviews>
		<?php
		$fileManager->includeTemplate( 'frontend/reviews.php', [
			'reviews'     => $reviews,
			'fileManager' => $fileManager
		] )
		?>
    </div>

    <form action="" data-create-review-form>
        <h2>Add new</h2>

        <input type="text" name="user_name" placeholder="user name"/>

        <select name="gender" id="">
            <option value="0">Male</option>
            <option value="1">Female</option>
        </select>

        <textarea name="props" id="" cols="30" rows="10" placeholder="Pros"></textarea>

        <textarea name="cons" id="" cols="30" rows="10" placeholder="Cons"></textarea>

        <select name="rating" id="">
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
            <option value="4">Four</option>
            <option value="5">Five</option>
        </select>

        <br>
        <br>
        <div class="g-recaptcha" data-sitekey="<?php echo $googleSiteKey; ?>"></div>
        <input type="submit" value="Create">
        <input type="hidden" name="related_post" value="<?php echo get_the_ID(); ?>">
        <input type="hidden" name="action" value="<?php echo AjaxHandler::CREATE_REVIEW_ACTION; ?>">
    </form>

    <button data-review-load-more>Load More</button>
</div>
