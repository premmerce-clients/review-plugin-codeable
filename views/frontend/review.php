<?php defined( "ABSPATH" ) || die;

use Codeable\Reviews\Review\Review;

/**
 * @var Review $review
 */

?>

<div class="review">
    <p><?php echo $review->getUserName() ?></p>
    <p><?php echo $review->getGender() == 1 ? 'Female' : 'Male'; ?></p>
    <p><?php echo $review->getCons() ?></p>
    <p><?php echo $review->getProps() ?></p>
    <p><?php echo $review->getRating() ?></p>
    <p><?php echo ( new DateTime() )->setTimestamp( $review->getPublishedTime() )->format( 'y-m-d h:i:s' ); ?></p>
    <p><b>likes: </b> <?php echo $review->getLikesCount(); ?></p>
    <button <?php echo !$review->isUserCanLike() ? 'disabled' : ''; ?> data-review-like-button data-review-like-id="<?php echo $review->getId() ?>">Like</button>
</div>
