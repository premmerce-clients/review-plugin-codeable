<?php defined( "ABSPATH" ) || die;

use Codeable\Reviews\Review\Review;
use Premmerce\SDK\V2\FileManager\FileManager;


/**
 * @var Review[] $reviews;
 * @var FileManager $fileManager
 */
?>

<?php foreach ( $reviews as $review ): ?>
	<?php $fileManager->includeTemplate( 'frontend/review.php', [
		'review' => $review
	] ); ?>
<?php endforeach; ?>
