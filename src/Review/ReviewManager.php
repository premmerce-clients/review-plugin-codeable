<?php namespace Codeable\Reviews\Review;

use Codeable\Reviews\ReviewsPlugin;
use DateTime;
use Exception;
use WP_Error;
use WP_Post;
use WP_Query;

class ReviewManager {

	/**
	 * Get review by id
	 *
	 * @param int $id
	 *
	 * @return bool|Review
	 */
	public static function get( $id ) {
		$reviewWP = get_post( $id );

		if ( $reviewWP ) {
			return self::postToReview( $reviewWP );
		}

		return false;
	}

	/**
	 * @param $postId
	 *
	 * @return float|int
	 */
	public static function getAverageRating( $postId ) {
		global $wpdb;

		$query = $wpdb->prepare( '
			SELECT AVG(average_rating.meta_value) as average_rating
			FROM ' . $wpdb->posts . '
			         INNER JOIN ' . $wpdb->postmeta . ' as related_post
			                    ON (wp_posts.ID = related_post.post_id AND related_post.meta_key = \'related_post\')
			         INNER JOIN ' . $wpdb->postmeta . ' as average_rating
			                    ON (wp_posts.ID = average_rating.post_id AND average_rating.meta_key = \'rating\')
			        INNER  JOIN ' . $wpdb->posts . ' as post ON (post.ID = related_post.meta_value)
			WHERE wp_posts.post_type = \'review\'
			AND related_post.meta_value = %d
			AND wp_posts.post_status = \'publish\'
			', $postId );

		return (float) $wpdb->get_col( $query )[0];
	}

	/**
	 * @param $postId
	 *
	 * @return array
	 */
	public static function getRatingData( $postId ) {
		global $wpdb;

		$query = $wpdb->prepare( '
			SELECT AVG(average_rating.meta_value) as average_rating, COUNT(average_rating.meta_value) as count, MAX(average_rating.meta_value) as best_rating
			FROM ' . $wpdb->posts . '
			         INNER JOIN ' . $wpdb->postmeta . ' as related_post
			                    ON (wp_posts.ID = related_post.post_id AND related_post.meta_key = \'related_post\')
			         INNER JOIN ' . $wpdb->postmeta . ' as average_rating
			                    ON (wp_posts.ID = average_rating.post_id AND average_rating.meta_key = \'rating\')
			        INNER  JOIN ' . $wpdb->posts . ' as post ON (post.ID = related_post.meta_value)
			WHERE wp_posts.post_type = \'review\'
			AND related_post.meta_value = %d
			AND wp_posts.post_status = \'publish\'
			', $postId );

		return $wpdb->get_row( $query, ARRAY_A );
	}

	/**
	 * @param int $postId
	 *
	 * @return float
	 */
	public static function getReviewsTotal( $postId ) {
		global $wpdb;

		$query = $wpdb->prepare( '
			SELECT COUNT(average_rating.meta_value) as count
			FROM ' . $wpdb->posts . '
			         INNER JOIN ' . $wpdb->postmeta . ' as related_post
			                    ON (wp_posts.ID = related_post.post_id AND related_post.meta_key = \'related_post\')
			         INNER JOIN ' . $wpdb->postmeta . ' as average_rating
			                    ON (wp_posts.ID = average_rating.post_id AND average_rating.meta_key = \'rating\')
			        INNER  JOIN ' . $wpdb->posts . ' as post ON (post.ID = related_post.meta_value)
			WHERE wp_posts.post_type = \'review\'
			AND related_post.meta_value = %d
			AND wp_posts.post_status = \'publish\'
			', $postId );

		return (float) $wpdb->get_col( $query )[0];
	}

	/**
	 * Get review for post
	 *
	 * @param int $postId
	 * @param int $perPage
	 * @param int $page
	 *
	 * @param string $orderBy
	 *
	 * @return Review[]
	 */
	public static function getForPost( $postId, $perPage = 10, $page = 1, $orderBy = 'date' ) {

		$args = [
			'post_type'      => CPT::POST_TYPE,
			'posts_per_page' => $perPage,
			'paged'          => $page,
			'order'          => 'ASC',
			'post_status'    => 'publish',
			'meta_query'     => [
				'related_post' => array(
					'key'   => 'related_post',
					'value' => $postId,
				),
			],
		];

		if ( $orderBy === 'date' ) {
			$args['orderby'] = 'publish_date';
		} else {
			$args['orderby']  = 'meta_value_num';
			$args['meta_key'] = '_thumbs_up_count';
			$args['order']    = 'DESC';
		}

		$query = new WP_Query( $args );

		return array_map( __CLASS__ . '::postToReview', $query->posts );
	}


	public static function getRatingCounts( $postId ) {
		global $wpdb;

		$query  = $wpdb->prepare( '
			SELECT average_rating.meta_value as rating, COUNT(average_rating.meta_value) as count
			FROM ' . $wpdb->posts . '
			         INNER JOIN ' . $wpdb->postmeta . ' as related_post
			                    ON (wp_posts.ID = related_post.post_id AND related_post.meta_key = \'related_post\')
			         INNER JOIN ' . $wpdb->postmeta . ' as average_rating
			                    ON (wp_posts.ID = average_rating.post_id AND average_rating.meta_key = \'rating\')
			        INNER  JOIN ' . $wpdb->posts . ' as post ON (post.ID = related_post.meta_value)
			WHERE wp_posts.post_type = \'review\'
			AND related_post.meta_value = %d
			AND wp_posts.post_status = \'publish\' GROUP BY average_rating.meta_value
			', $postId );

		$counts = [
			'1'     => 0,
			'2'     => 0,
			'3'     => 0,
			'4'     => 0,
			'5'     => 0,
			'total' => 0,
		];

		$totals = $wpdb->get_results( $query, ARRAY_A );

		foreach ( $totals as $total ) {
			$counts[ $total['rating'] ] = (int) $total['count'];
			$counts['total']            += (int) $total['count'];
		}

		return $counts;
	}


	/**
	 * Save review
	 *
	 * @param Review $review
	 *
	 * @param string $status
	 *
	 * @return Review
	 * @throws Exception
	 */
	public static function create( Review $review, $status = 'pending' ) {
		if ( is_null( $review->getId() ) ) {
			$postId = wp_insert_post( [
				'post_title'  => __( 'Review', 'review' ),
				'post_status' => $status,
				'post_type'   => CPT::POST_TYPE,
			] );

			if ( ! $postId instanceof WP_Error ) {

				update_field( 'rating', $review->getRating(), $postId );
				update_field( 'user_name', $review->getUserName(), $postId );
				update_field( 'props', $review->getProps(), $postId );
				update_field( 'cons', $review->getCons(), $postId );
				update_field( 'gender', $review->getGender(), $postId );
				update_field( 'related_post', $review->getRelatedPostId(), $postId );

				$review->setId( $postId );

				return $review;
			}

			throw new Exception( $postId->get_error_message() );
		}

		throw new Exception( 'Review already created' );
	}

	/**
	 * Map WP_Post data to Review instance
	 *
	 * @param WP_Post $post
	 *
	 * @return Review
	 */
	protected static function postToReview( WP_Post $post ) {
		$data = get_fields( $post->ID );

		$relatedPost = isset( $data['related_post'] ) ? $data['related_post'] : 0;
		$rating      = isset( $data['rating'] ) ? $data['rating'] : 5;
		$userName    = isset( $data['user_name'] ) ? $data['user_name'] : '';
		$props       = isset( $data['props'] ) ? $data['props'] : '';
		$cons        = isset( $data['cons'] ) ? $data['cons'] : '';
		$gender      = isset( $data['gender'] ) ? $data['gender'] : 0;
		$likes       = get_post_meta( $post->ID, '_thumbs_up', true );
		$likes       = $likes ? (array) $likes : [];
		$likesCount  = (int) get_post_meta( $post->ID, '_thumbs_up_count', true );

		try {
			$date = ( new DateTime( $post->post_date ) )->getTimestamp();
		} catch ( Exception $e ) {
			$date = 0;
		}

		$review = new Review( $rating, $userName, $cons, $props, $gender, $relatedPost );
		$review->setPublishedTime( $date );
		$review->setId( $post->ID );

		$review->setLikesCount( $likesCount );
		$review->setUserCanLike( ! in_array( ReviewsPlugin::getUserIp(), $likes ) );

		return $review;
	}

	/**
	 * Check if review is enabled on pages|posts
	 *
	 * @param string $type pages|posts
	 *
	 * @return bool
	 */
	public static function isEnabledOn( $type ) {

		$settings = get_option( 'reviews_settings', [] );

		switch ( $type ) {
			case 'post':
				return $settings['enable_on_posts'] ? (bool) $settings['enable_on_posts'] : false;
			case 'page':
				return $settings['enable_on_pages'] ? (bool) $settings['enable_on_pages'] : false;
			default:
				return false;
		}
	}

	/**
	 * @return string
	 */
	public static function getGoogleCaptchaSiteKey() {
		$settings = get_option( 'reviews_settings', [] );

		return isset( $settings['reviews_google_site_key'] ) ? $settings['reviews_google_site_key'] : '';
	}

	/**
	 * @return string
	 */
	public static function getGoogleCaptchaSecret() {
		$settings = get_option( 'reviews_settings', [] );

		return isset( $settings['reviews_google_site_secret'] ) ? $settings['reviews_google_site_secret'] : '';
	}

	/**
	 * @param int $postId
	 * @param string $ip
	 *
	 * @return bool
	 */
	public static function likeReviewIfAlreadyDont( $postId, $ip ) {
		$thumbsUps = get_post_meta( $postId, '_thumbs_up', true );
		$thumbsUps = $thumbsUps ? $thumbsUps : [];

		if ( in_array( $ip, $thumbsUps ) ) {
			return false;
		}

		$thumbsUps[] = $ip;

		update_post_meta( $postId, '_thumbs_up', $thumbsUps );
		update_post_meta( $postId, '_thumbs_up_count', count( $thumbsUps ) );

		return true;
	}
}