<?php namespace Codeable\Reviews\Review;

use Premmerce\SDK\V2\FileManager\FileManager;
use Premmerce\SDK\V2\Notifications\AdminNotifier;
use WP_Post;

/**
 * Class CPT
 * @package Codeable\Reviews\Review
 */
class CPT {

	const POST_TYPE = 'review';

	const SETTING_ACTION = 'review_settings_action';

	/**
	 * @var FileManager
	 */
	protected $fileManager;

	/**
	 * @var AdminNotifier
	 */
	private $adminNotifier;

	/**
	 * CPT constructor.
	 *
	 * @param FileManager $fileManager
	 * @param AdminNotifier $adminNotifier
	 */
	public function __construct( FileManager $fileManager, AdminNotifier $adminNotifier ) {

		$this->fileManager   = $fileManager;
		$this->adminNotifier = $adminNotifier;

		add_action( 'init', [ $this, 'register' ] );
		add_action( 'init', [ $this, 'registerACF' ] );
		add_action( 'admin_menu', [ $this, 'addSubPages' ] );
		add_action( 'admin_post_' . self::SETTING_ACTION, [ $this, 'handleSettings' ] );
		add_action( 'admin_menu', [ $this, 'addPendingReviewsCounter' ] );

		add_action( 'manage_' . self::POST_TYPE . '_posts_columns', [ $this, 'addReviewColumns' ] );
		add_action( 'manage_' . self::POST_TYPE . '_posts_custom_column', [ $this, 'reviewColumns' ], 2, 10 );

		add_filter( 'post_updated_messages', [ $this, 'reviewMessages' ] );

		add_action( 'post_submitbox_misc_actions', [ $this, 'showRelatedPost' ] );
	}

	/**
	 * Show related post notice
	 *
	 * @param WP_Post $post
	 */
	public function showRelatedPost( WP_Post $post ) {
		if ( $post->post_type === self::POST_TYPE ) {

			$relatedPost = (int) get_field( 'related_post', $post->ID );

			if ( $relatedPost > 0 ) {
				$relatedPost = get_post( $relatedPost );
			}

			if ( $relatedPost instanceof WP_Post ) {
				?>
                <div class="notice notice-info notice-alt inline">
                    <p>
						<?php _e( 'Review for', 'review' ) ?> <a
                                href="<?php echo get_edit_post_link( $relatedPost->ID ) ?>"
                                target="_blank"><?php echo $relatedPost->post_title; ?></a>
                    </p>
                </div>
				<?php
			}
		}
	}

	/**
	 * Add review columns
	 *
	 * @param array $columns
	 *
	 * @return array
	 */
	public function addReviewColumns( $columns ) {
		if ( isset( $columns['epcl_post_image'] ) ) {
			unset( $columns['epcl_post_image'] );
		}
		unset( $columns['date'] );

		$columns['related_post'] = __( 'Related post', 'review' );
		$columns['date']         = __( 'Review date', 'review' );
		$columns['rating']       = __( 'Rating', 'review' );

		return $columns;
	}

	/**
	 * Render review`s columns
	 *
	 * @param string $column
	 * @param int $postId
	 */
	public function reviewColumns( $column, $postId ) {
		if ( $column === 'rating' ) {
			$rating = get_field( 'rating', $postId );
			$rating = $rating ? (int) $rating : 5;

			echo str_repeat( '<svg fill="#32b4a8" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"><path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"/></svg>',
				$rating );
		}

		if ( $column === 'related_post' ) {
			$relatedPost = (int) get_field( 'related_post', $postId );
			if ( $relatedPost > 0 ) {
				$relatedPost = get_post( $relatedPost );
			}

			if ( $relatedPost ) {
				echo '<a href="' . get_edit_post_link( $relatedPost->ID ) . '" target="_blank">' . $relatedPost->post_title . '</a>';
			}
		}
	}

	/**
	 * Change messages for review
	 *
	 * @param array $messages
	 *
	 * @return array
	 */
	public function reviewMessages( $messages ) {
		global $post;

		if ( $post->post_type === self::POST_TYPE ) {
			$messages['post'][1] = __( 'Review updated', 'review' );
			$messages['post'][6] = __( 'Review created', 'review' );
		}

		return $messages;
	}

	/**
	 * Add pending reviews badge
	 */
	public function addPendingReviewsCounter() {
		global $menu;

		foreach ( $menu as $key => $value ) {

			if ( $menu[ $key ][2] == 'edit.php?post_type=' . self::POST_TYPE ) {

				$menu[ $key ][0] .= ' ' . $this->getPendingCountHtml();

				return;
			}
		}
	}

	/**
	 * Register method
	 */
	public function register() {
		register_post_type( self::POST_TYPE, [
			'labels'             => [
				'name'               => __( 'Reviews', 'reviews' ),
				'singular_name'      => __( 'Review', 'reviews' ),
				'add_new'            => __( 'Add new', 'reviews' ),
				'add_new_item'       => __( 'Add new review', 'reviews' ),
				'edit_item'          => __( 'Edit review', 'reviews' ),
				'new_item'           => __( 'New review', 'reviews' ),
				'view_item'          => __( 'View review', 'reviews' ),
				'search_items'       => __( 'Find review', 'reviews' ),
				'not_found'          => __( 'Review has not been found', 'reviews' ),
				'not_found_in_trash' => __( 'Not found in trash', 'reviews' ),
				'parent_item_colon'  => '',
				'menu_name'          => __( 'Reviews', 'hospital-location' ),
			],
			'public'             => false,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'rewrite'            => false,
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => [ 'title' ],
			'menu_icon'          => 'dashicons-welcome-write-blog'
		] );

	}

	public function registerACF() {
		new ACF();
	}

	/**
	 * Add review admin pages
	 */
	public function addSubPages() {
		add_submenu_page( 'edit.php?post_type=' . self::POST_TYPE, __( 'Settings', 'reviews' ),
			__( 'Settings', 'hospital-location' ), 'manage_options', self::POST_TYPE . '_settings', function () {

				$settings = get_option( 'reviews_settings', [] );

				$this->fileManager->includeTemplate( 'admin/review-settings.php', [
					'enableOnPages'    => isset( $settings['enable_on_pages'] ) ? (bool) $settings['enable_on_pages'] : false,
					'enableOnPosts'    => isset( $settings['enable_on_posts'] ) ? (bool) $settings['enable_on_posts'] : false,
					'googleSiteSecret' => isset( $settings['reviews_google_site_secret'] ) ? (string) $settings['reviews_google_site_secret'] : '',
					'googleSiteKey'    => isset( $settings['reviews_google_site_key'] ) ? (string) $settings['reviews_google_site_key'] : '',
				] );
			} );
	}

	/**
	 * Save settings
	 *
	 * @return bool
	 */
	public function handleSettings() {

		if ( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], self::SETTING_ACTION ) ) {

			$settings = [
				'enable_on_posts'            => isset( $_POST['reviews_enable_on_posts'] ),
				'enable_on_pages'            => isset( $_POST['reviews_enable_on_pages'] ),
				'reviews_google_site_key'    => isset( $_POST['reviews_google_site_key'] ) ? sanitize_text_field( $_POST['reviews_google_site_key'] ) : '',
				'reviews_google_site_secret' => isset( $_POST['reviews_google_site_secret'] ) ? sanitize_text_field( $_POST['reviews_google_site_secret'] ) : '',
			];

			update_option( 'reviews_settings', $settings );

			$this->adminNotifier->flash( __( 'Options have been updated successfully', 'review' ),
				AdminNotifier::SUCCESS );

		} else {
			$this->adminNotifier->flash( __( 'Invalid nonce', 'review', AdminNotifier::ERROR ) );
		}

		return wp_safe_redirect( wp_get_referer() );
	}

	/**
	 * Get badge with pending reviews
	 *
	 * @return string
	 */
	protected function getPendingCountHtml() {

		$count = (int) wp_count_posts( self::POST_TYPE )->pending;

		if ( $count !== 0 ) {
			return '<span class="update-plugins count-5"><span class="plugin-count">' . $count . '</span></span>';
		}

		return '';
	}
}