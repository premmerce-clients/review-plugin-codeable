<?php namespace Codeable\Reviews\Review;

class Review {

	const MALE_GANDER = 0;

	const FEMALE_GANDER = 1;

	/**
	 * @var int
	 */
	protected $id = null;

	/**
	 * @var int
	 */
	protected $relatedPostId = null;

	/**
	 * @var int
	 */
	protected $rating;

	/**
	 * @var string
	 */
	protected $userName;

	/**
	 * @var string
	 */
	protected $cons;

	/**
	 * @var string
	 */
	protected $props;

	/**
	 * @var int
	 */
	protected $gender;

	/**
	 * @var int
	 */
	protected $publishedTime;

	/**
	 * @var bool
	 */
	protected $userCanLike;

	/**
	 * @var int
	 */
	protected $likesCount;

	/**
	 * Review constructor.
	 *
	 * @param int $rating
	 * @param string $userName
	 * @param string $cons
	 * @param string $props
	 * @param int $gender
	 * @param $relatedPostId
	 */
	public function __construct( $rating, $userName, $cons, $props, $gender, $relatedPostId ) {
		$this->rating        = $rating;
		$this->userName      = $userName;
		$this->cons          = $cons;
		$this->props         = $props;
		$this->gender        = $gender;
		$this->relatedPostId = $relatedPostId;
	}

	/**
	 * @return bool
	 */
	public function isUserCanLike() {
		return $this->userCanLike;
	}

	/**
	 * @param bool $userCanLike
	 */
	public function setUserCanLike( $userCanLike ) {
		$this->userCanLike = $userCanLike;
	}

	/**
	 * @return int
	 */
	public function getLikesCount() {
		return $this->likesCount;
	}

	/**
	 * @param int $likesCount
	 */
	public function setLikesCount( $likesCount ) {
		$this->likesCount = $likesCount;
	}

	/**
	 * @param int $publishedTime
	 */
	public function setPublishedTime( $publishedTime ) {
		$this->publishedTime = $publishedTime;
	}

	/**
	 * @return int
	 */
	public function getPublishedTime() {
		return $this->publishedTime;
	}

	/**
	 * @return  int $relatedPostId
	 */
	public function getRelatedPostId() {
		return $this->relatedPostId;
	}

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getRating() {
		return $this->rating;
	}

	/**
	 * @return string
	 */
	public function getUserName() {
		return $this->userName;
	}

	/**
	 * @return string
	 */
	public function getCons() {
		return $this->cons;
	}

	/**
	 * @return string
	 */
	public function getProps() {
		return $this->props;
	}

	/**
	 * @return int
	 */
	public function getGender() {
		return $this->gender;
	}

}