<?php namespace Codeable\Reviews;

use Codeable\Reviews\Review\Review;
use Codeable\Reviews\Review\ReviewManager;
use Exception;
use Premmerce\SDK\V2\FileManager\FileManager;
use ReCaptcha\ReCaptcha;

class AjaxHandler {

	const CREATE_REVIEW_ACTION = 'create_review';
	const LOAD_MORE_ACTION = 'load_more_reviews';
	const LIKE_REVIEW_ACTION = 'like_review';

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * AjaxHandler constructor.
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct( FileManager $fileManager ) {

		$this->fileManager = $fileManager;

		add_action( 'wp_ajax_nopriv_' . self::CREATE_REVIEW_ACTION, [ $this, 'createReview' ] );
		add_action( 'wp_ajax_' . self::CREATE_REVIEW_ACTION, [ $this, 'createReview' ] );

		add_action( 'wp_ajax_nopriv_' . self::LOAD_MORE_ACTION, [ $this, 'loadMore' ] );
		add_action( 'wp_ajax_' . self::LOAD_MORE_ACTION, [ $this, 'loadMore' ] );

		add_action( 'wp_ajax_nopriv_' . self::LIKE_REVIEW_ACTION, [ $this, 'likeReview' ] );
		add_action( 'wp_ajax_' . self::LIKE_REVIEW_ACTION, [ $this, 'likeReview' ] );
	}

	/**
	 * Like ajax handler
	 */
	public function likeReview() {
		$reviewId  = isset( $_POST['postId'] ) ? intval( $_POST['postId'] ) : false;
		$ipAddress = ReviewsPlugin::getUserIp();

		ReviewManager::likeReviewIfAlreadyDont( $reviewId, $ipAddress );

		wp_send_json( [
			'success' => true,
		] );
	}

	/**
	 * Load more ajax handler
	 */
	public function loadMore() {

		$perPage = 2;

		$loadPage = isset( $_POST['loadPage'] ) ? intval( $_POST['loadPage'] ) : false;
		$postId   = isset( $_POST['postId'] ) ? intval( $_POST['postId'] ) : false;
		$orderBy  = isset( $_POST['orderBy'] ) ? $_POST['orderBy'] : 'date';
		$orderBy  = $orderBy === 'rating' ? $orderBy : 'date';

		if ( ! $loadPage || ! $postId ) {
			wp_send_json( [
				'needLoadMore' => false,
				'error'        => __( 'Invalid Params', 'review' ),
			] );
		}

		$reviews = ReviewManager::getForPost( $postId, $perPage, $loadPage, $orderBy );

		if ( count( $reviews ) > 0 ) {
			$response = [
				'needLoadMore' => true,
				'reviewsHtml'  => $this->renderReviews( $reviews )
			];
		} else {
			$response = [
				'needLoadMore' => false,
				'reviewsHtml'  => ''
			];
		}

		wp_send_json( $response );
	}

	/**
	 * Return rendered reviews HTML
	 *
	 * @param array $reviews
	 *
	 * @return string
	 */
	protected function renderReviews( $reviews ) {
		return $this->fileManager->renderTemplate( 'frontend/reviews.php', [
			'reviews'     => $reviews,
			'fileManager' => $this->fileManager,
		] );
	}

	/**
	 * Create review ajax handler
	 */
	public function createReview() {

		$data = $this->sanitizeCreateReviewRequest( $_POST );

		$errors = $this->validateCreateReviewRequest( $data );

		if ( ! empty( $errors ) ) {
			wp_send_json( [ 'errors' => $errors ] );
		}

		$review = new Review( $data['rating'], $data['user_name'], $data['cons'], $data['props'], $data['gender'],
			$data['related_post'] );

		try {
			ReviewManager::create( $review );
		} catch ( Exception $e ) {
			wp_send_json( [ 'errors' => $e->getMessage() ] );
		}

		wp_send_json( [ 'success' => true ] );
	}

	/**
	 * Validate request
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	protected function validateCreateReviewRequest( $data ) {
		$errors = [];

		if ( ! $this->validateCaptcha( $data['captcha'] ) ) {
			$errors['captcha'] = __( 'Invalid captcha', 'review' );
		}

		if ( empty( $data['user_name'] ) ) {
			$errors['user_name'] = __( 'Invalid username', 'review' );
		}

		if ( empty( $data['cons'] ) ) {
			$errors['cons'] = __( 'Invalid cons', 'review' );
		}

		if ( empty( $data['props'] ) ) {
			$errors['props'] = __( 'Invalid props', 'review' );
		}

		if ( $data['gender'] === false || $data['gender'] > 1 ) {
			$errors['gender'] = __( 'Invalid gender', 'review' );
		}

		if ( $data['rating'] === false || $data['rating'] > 5 ) {
			$errors['rating'] = __( 'Invalid rating', 'review' );
		}

		$post = get_post( $data['related_post'] );

		if ( $data['related_post'] === false || ! $post || ! ReviewManager::isEnabledOn( $post->post_type ) ) {
			$errors['related_post'] = __( 'Invalid related post', 'review' );
		}

		return $errors;
	}

	/**
	 * Sanitize request
	 *
	 * @param array $data
	 *
	 * @return array
	 */
	protected function sanitizeCreateReviewRequest( $data ) {
		$review = [
			'captcha'      => isset( $data['g-recaptcha-response'] ) ? $data['g-recaptcha-response'] : '',
			'user_name'    => isset( $data['user_name'] ) ? substr( sanitize_text_field( $data['user_name'] ), 0,
				50 ) : '',
			'cons'         => isset( $data['cons'] ) ? substr( sanitize_text_field( $data['cons'] ), 0, 1000 ) : '',
			'props'        => isset( $data['props'] ) ? substr( sanitize_text_field( $data['props'] ), 0, 1000 ) : '',
			'gender'       => isset( $data['gender'] ) ? absint( $data['gender'] ) : false,
			'rating'       => isset( $data['rating'] ) ? absint( $data['rating'] ) : false,
			'related_post' => isset( $data['related_post'] ) ? absint( $data['related_post'] ) : false,
		];

		return $review;
	}

	/**
	 * Validate google captcha
	 *
	 * @param string $captcha
	 *
	 * @return bool
	 */
	protected function validateCaptcha( $captcha ) {
		$recaptcha = new ReCaptcha( ReviewManager::getGoogleCaptchaSecret() );
		$resp      = $recaptcha->verify( $captcha );

		return $resp->isSuccess();
	}
}