<?php namespace Codeable\Reviews\Frontend;

use Codeable\Reviews\AjaxHandler;
use Codeable\Reviews\Review\ReviewManager;
use Premmerce\SDK\V2\FileManager\FileManager;

/**
 * Class Frontend
 *
 * @package Codeable\Reviews\Frontend
 */
class Frontend {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * Frontend constructor.
	 *
	 * @param FileManager $fileManager
	 */
	public function __construct( FileManager $fileManager ) {
		$this->fileManager = $fileManager;

		add_action( 'wp_head', [ $this, 'addCaptchaScript' ] );
		add_action( 'wp_head', [ $this, 'renderJsonLdRating' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueueScripts' ] );
		add_action( 'get_template_part_partials/author-box', [ $this, 'renderReviews' ] );
	}

	public function renderJsonLdRating() {

		global $post;

		if ( $this->isEnableForPage() ) {

			$ratingData = ReviewManager::getRatingData( $post->ID );

			if ( ! empty( $ratingData['best_rating'] ) && ! empty( $ratingData['count'] ) && ! empty( $ratingData['average_rating'] ) ) {
				?>
                <script type="application/ld+json">
                {
                    "@context": "http://schema.org",
                    "@type": "article",
                    "aggregateRating": {
                        "@type": "AggregateRating",
                        "bestRating": "<?php echo $ratingData['best_rating']; ?>",
                        "ratingCount": "<?php echo $ratingData['count']; ?>",
                        "ratingValue": "<?php echo $ratingData['average_rating']; ?>"
                    }
                }


                </script>
				<?php
			}
		}
	}

	/**
	 * Enqueue app assets
	 */
	public function enqueueScripts() {
		if ( $this->isEnableForPage() ) {

			global $post;

			wp_register_script( 'reviews_main_script', $this->fileManager->locateAsset( 'frontend/main.js' ),
				[ 'jquery' ] );

			wp_localize_script( 'reviews_main_script', 'reviewsConfig', [
				'loadMoreAction'     => AjaxHandler::LOAD_MORE_ACTION,
				'createReviewAction' => AjaxHandler::CREATE_REVIEW_ACTION,
				'likeReviewAction'   => AjaxHandler::LIKE_REVIEW_ACTION,
				'ajaxUrl'            => admin_url( "admin-ajax.php" ),
				'currentPostId'      => $post->ID,
			] );

			wp_enqueue_script( 'reviews_main_script' );
		}

	}

	/**
	 * Add google captcha script
	 */
	public function addCaptchaScript() {
		if ( $this->isEnableForPage() ) {
			?>
            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
			<?php
		}
	}

	/**
	 * Render review
	 */
	public function renderReviews() {
		global $post;

		$orderBy = isset( $_GET['review_sort_by'] ) ? $_GET['review_sort_by'] : '';
		$orderBy = $orderBy === 'rating' ? $orderBy : 'date';

		if ( ReviewManager::isEnabledOn( $post->post_type ) ) {
			$this->fileManager->includeTemplate( 'frontend/post-reviews.php', [
				'reviews'       => ReviewManager::getForPost( $post->ID, 2, null, $orderBy ),
				'averageRating' => ReviewManager::getAverageRating( $post->ID ),
				'googleSiteKey' => ReviewManager::getGoogleCaptchaSiteKey(),
				'totals'        => ReviewManager::getRatingCounts( $post->ID ),
				'fileManager'   => $this->fileManager,
			] );
		}
	}

	/**
	 * Check if reviews is enable on page
	 *
	 * @return bool
	 */
	protected function isEnableForPage() {

		global $post;

		return is_single() && ReviewManager::isEnabledOn( $post->post_type );
	}
}