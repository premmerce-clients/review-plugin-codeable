<?php namespace Codeable\Reviews;

use Codeable\Reviews\Review\CPT;
use Codeable\Reviews\Review\ReviewManager;
use Premmerce\SDK\V2\FileManager\FileManager;
use Codeable\Reviews\Frontend\Frontend;
use Premmerce\SDK\V2\Notifications\AdminNotifier;

/**
 * Class ReviewsPlugin
 *
 * @package Codeable\Reviews
 */
class ReviewsPlugin {

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * @var AdminNotifier
	 */
	private $adminNotifier;

	/**
	 * @var CPT
	 */
	private $reviewCTP;

	/**
	 * @var AjaxHandler
	 */
	private $ajaxHandler;

	/**
	 * ReviewsPlugin constructor.
	 *
	 * @param string $mainFile
	 */
	public function __construct( $mainFile ) {

		$this->fileManager   = new FileManager( $mainFile );
		$this->adminNotifier = new AdminNotifier();
		$this->reviewCTP     = new CPT( $this->fileManager, $this->adminNotifier );
		$this->ajaxHandler   = new AjaxHandler( $this->fileManager );

		add_action( 'plugins_loaded', [ $this, 'loadTextDomain' ] );

	}

	/**
	 * Run plugin part
	 */
	public function run() {
		if ( ! is_admin() ) {
			new Frontend( $this->fileManager );
		}
	}

	/**
	 * Load plugin translations
	 */
	public function loadTextDomain() {
		$name = $this->fileManager->getPluginName();
		load_plugin_textdomain( 'reviews', false, $name . '/languages/' );
	}

	public static function getUserIp() {
		if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			//ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			//ip pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}
}