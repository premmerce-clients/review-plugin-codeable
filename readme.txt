=== Reviews ===
Contributors: codeable, premmerce
Tags: tags
Requires at least: 5.2.2
Tested up to: 5.2.2
Stable tag: 1.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows visitors to add user reviews and ratings.

== Description ==


== Installation ==

1. Unzip the downloaded zip file.
1. Upload the plugin folder into the `wp-content/plugins/` directory of your WordPress site.
1. Activate `Reviews` from Plugins page

== Changelog ==

= 1.0 =

Release Date: Aug 12, 2019

* Initial release