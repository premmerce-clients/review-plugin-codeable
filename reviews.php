<?php

use Codeable\Reviews\ReviewsPlugin;

/**
 *
 * Plugin Name:       Reviews
 * Description:       Allows visitors to add user reviews and ratings.
 * Version:           1.0
 * Author:            codeable
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       reviews
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

call_user_func( function () {

	require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

	$main = new ReviewsPlugin( __FILE__ );

	$main->run();
} );